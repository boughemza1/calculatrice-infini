#include <iostream>
#include <fstream>
#include "ast-node.hh"
#include "bignum.hh"
#include "base.hh"
#include "number-node.hh"
#include "bin-op-node.hh"
#include "un-op-node.hh"
#include "ast-factory.hh"

int main()
{
    auto num = bistro::BigNum<uint8_t>(10);
    auto other = num.clone();
    num.set_digit(1, 1);
    num.set_digit(2, 2);
    num.set_digit(3, 6);
    num.set_digit(4, 8);
    std::cout << std::to_string(num.get_digit(1)) << " ";
    std::cout << std::to_string(num.get_digit(2)) << " ";
    std::cout << std::to_string(num.get_digit(3)) << " ";
    std::cout << std::to_string(num.get_digit(4)) << std::endl;
    std::cout << "num > num " << (num > num) << " ";
    std::cout << "num == num " << (num == num) << " ";
    std::cout << "num == clone " << (num == other) << " ";
    std::cout << "positive: " << num.is_positive() << std::endl;
    num.set_positive(true);
    std::cout << "positive: " << num.is_positive() << std::endl;


    auto base = bistro::Base<uint8_t, char>();
    base.add_digit('0');
    base.add_digit('1');
    base.add_digit('2');
    base.add_digit('3');
    base.add_digit('4');
    base.add_digit('5');
    base.add_digit('6');
    base.add_digit('7');
    base.add_digit('8');
    base.add_digit('9');
    std::cout << std::to_string(base.get_char_value('1')) << std::endl;
    std::cout << base.get_digit_representation(1) << std::endl;
    std::cout << std::to_string(base.get_char_value('2')) << std::endl;
    std::cout << std::to_string(base.get_char_value('3')) << std::endl;
    std::cout << std::to_string(base.get_char_value('4')) << std::endl;



    std::ifstream file("test.in");
    //AST TEST
    auto ast = bistro::ASTFactory<bistro::BigNum<uint8_t>, bistro::Base<uint8_t, char>>();
    auto base_read = ast.read_base(file);
    file.get();
    auto tree = ast.read_AST(file, base_read);
    tree->print_infix(std::cout, base_read);
    std::cout << std::endl;
    auto bignum = tree->eval();
    //END AST TEST
    bignum->print(std::cout, base_read);
    std::cout << std::endl;
    num.print(std::cout, base) << std::endl;
    auto l = num - num;
    l.print(std::cout, base) << std::endl;
    return 0;
}
