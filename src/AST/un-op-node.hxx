#include <ostream>
#include "un-op-node.hh"

namespace bistro
{
template <typename BigNum, typename Base>
std::ostream& UnOpNode<BigNum, Base>::print_infix(std::ostream& out, const Base& b) const
{

    if (positive_ == '+')
        out << "(+";
    else
        out << "(-";

    if (child_)
        child_->print_infix(out, b);
    out << ")";
    return out;
}
}
