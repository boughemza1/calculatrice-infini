#pragma once
#include "ast-node.hh"
#include <ostream>
namespace bistro
{
template <typename BigNum, typename Base>
class UnOpNode : public ASTNode<BigNum, Base>
{
    public:
        using node_ptr = std::shared_ptr<ASTNode<BigNum, Base>>;
            UnOpNode(char positive, node_ptr child)
            :positive_(positive)
             , child_(child)
         {}

        bool is_positive() const
        {
            return positive_ == '+';
        }

        std::ostream& print_infix(std::ostream& out, const Base& b ) const override;

        std::shared_ptr<BigNum> eval() const override
        {
            auto e = child_->eval();
            e->set_positive(is_positive());
            return std::make_shared<BigNum>(e->clone());
        }

    private:
        char positive_;
        node_ptr child_;
};
}
#include "un-op-node.hxx"
