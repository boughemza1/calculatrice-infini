#pragma once
#include "ast-node.hh"
#include <memory>
#include <iostream>


namespace bistro
{
template <typename BigNum, typename Base>
class BinOpNode : public ASTNode<BigNum, Base>
{
    public:
        using node_ptr = std::shared_ptr<ASTNode<BigNum, Base>>;
        BinOpNode(char op, node_ptr child, node_ptr child2)
            : op_(op)
            , child_(child)
            , child2_(child2)
        {}

        inline char get_operator() const
        {
            return op_;
        }
        std::ostream&
        print_infix(std::ostream& out, const Base& b ) const override;

        std::shared_ptr<BigNum> eval() const override
        {
            auto e = child_->eval();
            auto e2 = child2_->eval();
            if (op_ == '+')
                return std::make_shared<BigNum>(*e + *e2);
            else if (op_ == '-')
                return std::make_shared<BigNum>(*e - *e2);
            else if (op_ == '*')
                return std::make_shared<BigNum>(*e * *e2);
            else if (op_ == '/')
                return std::make_shared<BigNum>(*e / *e2);
            else
                return std::make_shared<BigNum>(*e % *e2);
        }

    private:
        char op_;
        node_ptr child_;
        node_ptr child2_;
};
#include "bin-op-node.hxx"
}
