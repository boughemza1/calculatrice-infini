#pragma once
#include "bignum.hh"
#include "ast-node.hh"
#include <ostream>
#include <memory>
#include <string>


using string = std::string;

namespace bistro
{
template <typename BigNum, typename Base>
class NumberNode : public ASTNode<BigNum, Base>
{
    public:
        NumberNode(std::ifstream& in, const Base& b)
            : num_(in, b)
        {}

        inline BigNum get_num() const
        {
            return num_;
        }
        inline std::ostream&
        print_infix(std::ostream& out, const Base& b ) const override
        {
            num_.print(out, b);
            return out;
        }

        std::shared_ptr<BigNum> eval() const override
        {
            return std::make_shared<BigNum>(num_.clone());
        }
    private:
        BigNum num_;
};
}
