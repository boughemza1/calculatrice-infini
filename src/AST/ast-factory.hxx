#include "ast-factory.hh"
#include "ast-node.hh"
#include "un-op-node.hh"
#include "number-node.hh" //added after dementor 1
#include "bin-op-node.hh"

#include <fstream>
#include <utility>
#include <stack>
#include <memory> //addedn after dementor 1
#include <string>

using string = std::string;
namespace bistro
{

bool in_par = false;
template <typename BigNum, typename Base>
std::shared_ptr<ASTNode<BigNum, Base>> Exp(std::ifstream& in, const Base& base);

template <typename BigNum, typename Base>
std::shared_ptr<ASTNode<BigNum, Base>> T(std::ifstream& in, const Base& base);

template <typename BigNum, typename Base>
std::shared_ptr<ASTNode<BigNum, Base>> F(std::ifstream& in, const Base& base);

template <typename BigNum, typename Base>
Base ASTFactory<BigNum, Base>::read_base(std::ifstream& in)
{
    auto base = Base();
    int size;
    typename Base::char_t  c;
    if (in.peek() <= '0' && in.peek() >= '9')
        throw std::domain_error("Invalid Input [Base]");
    in >> size;
    while (size > 0 && (in >> c).good())
    {
        try
        {
            base.add_digit(c);
            size--;
            if (in.peek() == '\n')
                break;
        }
        catch (std::invalid_argument& e)
        {
            throw std::domain_error("Invalid Input");
        }
    }
    if (size > 0 || in.peek() != '\n')
        throw std::domain_error("Invalid Input");
    return base;
}

template <typename BigNum, typename Base>
std::shared_ptr<ASTNode<BigNum, Base>> F(std::ifstream& in, const Base& base)
{
    auto c = in.peek();
    if (base.is_digit(c))
    {
        auto e =std::make_shared<NumberNode<BigNum, Base>>(in, base);
        in.unget();
        return e;
    }
    else if (c == '(')
    {
        in_par = true;
        in.get();
        auto e = Exp<BigNum, Base>(in, base);
        if (in.peek() == ')')
        {
            in_par = false;
            in.get();
            return e;
        }
        else
            return nullptr;
    }
    else if (c == '-'  || c == '+')
    {
        in.get();
        auto e = F<BigNum, Base>(in, base);
        if (!e)
            return nullptr;
        //else if (in.peek() == -1 || in.peek() == '\n' || in_par)
            return std::make_shared<UnOpNode<BigNum, Base>>(c, e);
        //return nullptr;
    }
    return nullptr;
}

template <typename BigNum, typename Base>
std::shared_ptr<ASTNode<BigNum, Base>> T(std::ifstream& in, const Base& base)
{
    std::shared_ptr<ASTNode<BigNum, Base>> e1 = nullptr;
    std::shared_ptr<ASTNode<BigNum, Base>> e2 = nullptr;
    if (in.peek() != -1)
    {
        e1 = F<BigNum, Base>(in, base);
    }
    if (!e1)
        return nullptr;
    auto c = in.peek();
    if (c != -1 && base.is_operator(c))
    {
        if (c == '+' || c == '-')
            return e1;
        in.get();
        e2 = T<BigNum, Base>(in, base);
    }
    else
        return e1;
    if (!e2)
        return nullptr;
    //if (in.peek() == in.eof())
        return std::make_shared<BinOpNode<BigNum, Base>>(c, e1, e2);
    //return nullptr;
}



template <typename BigNum, typename Base>
std::shared_ptr<ASTNode<BigNum, Base>> Exp(std::ifstream& in, const Base& base)
{
    std::shared_ptr<ASTNode<BigNum, Base>> e1 = nullptr;
    std::shared_ptr<ASTNode<BigNum, Base>> e2 = nullptr;
    if (in.peek() != -1)
        e1 = T<BigNum, Base>(in, base);
    if (e1 == nullptr)
        return nullptr;
    auto c = in.peek();
    if (base.is_operator(c))
    {
        in.get();
        e2 = Exp<BigNum, Base>(in, base);
    }
    else
        return e1;
    if (e2 == nullptr)
        return nullptr;
    if (in.peek() != -1 && in.peek() != '\n' && !in_par)
        return nullptr;
    return std::make_shared<BinOpNode<BigNum, Base>>(c, e1, e2);
}



template <typename BigNum, typename Base>
std::shared_ptr<ASTNode<BigNum, Base>> ASTFactory<BigNum, Base>::read_AST
(std::ifstream& in, const base_t& base)
{
    auto e = Exp<BigNum, Base>(in, base);
    if (!e)
        throw std::domain_error("Wrong Input");
    else
        return e;
}

}
