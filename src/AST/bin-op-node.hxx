#include "bin-op-node.hh"
#include <ostream>



template <typename BigNum, typename Base>
std::ostream&
BinOpNode<BigNum, Base>::print_infix(std::ostream& out, const Base& b) const
{
    out << '(';
    if (child_)
        child_->print_infix(out, b);
    out << op_;
    if (child2_)
        child2_->print_infix(out, b);
    out << ')';
    return out;
}
