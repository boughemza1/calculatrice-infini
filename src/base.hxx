#include "base.hh"

#include <cstdint>
#include <initializer_list>

#include <unordered_map>

template <typename Value, typename Char>
Base<Value, Char>::Base()
    : digits_(std::unordered_map<Char, Value>())
    , base_num_(0)
{}

template <typename Value, typename Char>
Base<Value, Char>::Base(std::initializer_list<Char> list)
    : digits_(std::unordered_map<Char, Value>())
{
    size_t j = 0;
    Value i = 0;
    for (auto tmp: list)
    {
        j++;
        digits_[tmp] = i;
        i++;
    }
    base_num_ = j;
}

template <typename Value, typename Char>
bool Base<Value, Char>::is_operator(Char c)
{
    return c == '*' || c == '+' || c  == '-' || c == '/'
           || c == '%';

}

template <typename Value, typename Char>
size_t Base<Value, Char>::get_base_num() const
{
    return base_num_;
}

template <typename Value, typename Char>
void Base<Value, Char>::add_digit(Char repr)
{
    if ( repr == '*' || repr == '+' || repr  == '-' || repr == '/'
        || repr == '%')
        throw std::invalid_argument("It's an operator");
    if (digits_.find(repr) != digits_.end())
        throw std::invalid_argument("Already matched");
    digits_.emplace_hint(digits_.end(), repr, base_num_);
    base_num_++;
}

template <typename Value, typename Char>
bool Base<Value, Char>::is_digit(char_t c) const
{
    auto tmp = digits_.find(c);
    return tmp != digits_.end();
}

template <typename Value, typename Char>
Char Base<Value, Char>::get_digit_representation(value_t i) const
{
    for (auto tmp = digits_.begin(); tmp != digits_.end(); ++tmp)
    {
        if (tmp->second == i)
            return tmp->first;
    }
    throw std::out_of_range("The searched value is not in the base");
}

template <typename Value, typename Char>
Value Base<Value, Char>::get_char_value(char_t r) const
{
    auto tmp = digits_.find(r);
    if (tmp == digits_.end())
        throw std::out_of_range("The searched value is not in the base");
    return tmp->second;
}

