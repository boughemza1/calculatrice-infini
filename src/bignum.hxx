#pragma once
#include <cstdint>
#include <exception>
#include <stdexcept>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <memory>
#include "bignum.hh"

namespace bistro
{
template <typename T>
BigNum<T>::BigNum(std::size_t base)
    : base_(base)
    , number_(std::vector<T>())
    , index_(0)
      , positive_(true)
{}

template <typename T>
BigNum<T>::BigNum(std::size_t base, std::vector<T>  number,
        size_t index, bool positive)
    : base_(base)
    , number_(number)
    , index_(index)
      , positive_(positive)
{}

template <typename T>
template <typename Base>
BigNum<T>::BigNum(std::ifstream& in, const Base& b)
    : number_()
{
   typename Base::char_t c;
   positive_ = true;
   size_t i = 0;
   base_ = b.get_base_num();
   for (;(in >> c).good(); i++)
   {
       if (!b.is_operator(c) && c != '(' && c != ')')
       {
           typename Base::value_t tmp = b.get_char_value(c);
           number_.push_back(tmp);
       }
       else break;
   }
   index_ = i;
   std::reverse(number_.begin(), number_.end());
    while (index_ - 1 > 0 && number_[index_ - 1] == 0)
    {
        number_.pop_back();
        index_--;
    }
    if (index_ == 1 && number_[index_ - 1] == 0)
    {
        index_--;
        number_.pop_back();
    }
}

template <typename T>
BigNum<T> BigNum<T>::clone() const
{
    return BigNum<T>(base_, number_, index_, positive_);
}

template <typename T>
size_t BigNum<T>::get_num_digits() const
{
    return index_;
}

template <typename T>
T BigNum<T>::get_digit(index_t i) const
{
    if (i >= index_)
        throw std::out_of_range("out of vector");
    return number_.at(i);
}

template <typename T>
void BigNum<T>::set_digit(size_t i,T d)
{
    if (d < 0)
        throw std::invalid_argument("Set digit: invalid arg");
    size_t d2 = d;
    if (d2 >= base_)
        throw std::invalid_argument("Set digit: invalid arg");
    for (auto j = index_; j <= i ; j++)
    {
        index_++;
        number_.push_back(0);
    }
    number_[i] = d;
    while (index_ - 1 > 0 && number_[index_ - 1] == 0)
    {
        number_.pop_back();
        index_--;
    }
    if (index_ == 1 && number_[index_ - 1] == 0)
    {
        index_--;
        number_.pop_back();
    }
}

template <typename T>
bool BigNum<T>::is_positive() const
{
    return positive_;
}

template <typename T>
void BigNum<T>::set_positive(bool p)
{
    if (!positive_ && !p)
        positive_ = true;
    else
        positive_ = p;
}

template <typename T>
bool BigNum<T>::operator>(const self_t& other) const
{
    if (base_ > other.base_)
        return true;
    if (base_ < other.base_)
        return false;
    if (index_ > other.index_)
        return positive_;
    if (index_ < other.index_)
        return !other.positive_;
    if (positive_ == other.is_positive())
    {
        int index = index_ - 1;
        int i = index_ - 1;
        for(; i >= 0 && number_[i] == other.number_[i];
                i--)
            continue;
        if (i == index - 1)
            return false;
        if (positive_ == false)
            return number_[i] < other.number_[i];
        return number_[i] > other.number_[i];
    }
    return positive_;
}

template <typename T>
bool BigNum<T>::operator==(const self_t& other) const
{
    if (positive_ != other.positive_)
        return false;
    if (index_ != other.index_)
        return false;
    size_t i = 0;
    while (i < index_ && number_[i] == other.number_[i])
        i++;
    return i == index_;
}

template <typename T>
BigNum<T>::operator bool() const
{
    return !number_.empty();
}

template <typename T>
template <typename Base>
std::ostream& BigNum<T>::print(std::ostream& out, const Base& b) const
{
    if (b.get_base_num() != base_)
        throw std::invalid_argument("Bases don't have the same size");
    if (number_.empty())
        out << b.get_digit_representation(0);
    else if (!positive_)
        out << '-';
    for (int j = index_ - 1; j >= 0; j--)
        out << b.get_digit_representation(number_[j]);
    return out;
}


template <typename T>
BigNum<T> BigNum<T>::operator-(const BigNum<T>& other) const
{
    if (other.base_ != base_)
       throw std::invalid_argument("Base different");
    else if(!positive_)
    {
        auto num = other.clone();
        num.set_positive(false);
        return *this + num;
    }
    else if (!other.positive_)
    {
        auto num = other.clone();
        num.set_positive(true);
        return *this + num;
    }

    auto num = BigNum(base_);
    T retenu = 0;
    size_t j = 0;
    if (j < index_ && *this > other)
    {
       for (; j < index_; j++)
       {
           auto tmp = number_[j] - retenu;
           if (j < other.index_)
               tmp -= other.number_[j];
           if (tmp < 0)
           {
               tmp += base_;
               retenu = 1;
           }
           else
               retenu = 0;
           num.set_digit(j, tmp);
       }
    }
    else
    {
       for (; j < other.index_; j++)
       {
           auto tmp = other.number_[j] - retenu;
           if (j < index_)
               tmp -= number_[j];
           if (tmp < 0)
           {
               tmp += base_;
               retenu = 1;
           }
           else
               retenu = 0;
           num.set_digit(j, tmp);
       }
       num.set_positive(false);
    }
    return num;
}


template <typename T>
BigNum<T>& BigNum<T>::operator+=(const BigNum<T>& other)
{
   *this = *this + other;
   return *this;
}

template <typename T>
BigNum<T>& BigNum<T>::operator-=(const BigNum<T>& other)
{
   *this = *this - other;
   return *this;
}

template <typename T>
BigNum<T>& BigNum<T>::operator*=(const BigNum<T>& other)
{
   *this = *this * other;
   return *this;
}


template <typename T>
BigNum<T>& BigNum<T>::operator%=(const BigNum<T>& other)
{
   *this = *this % other;
   return *this;
}


template <typename T>
BigNum<T>& BigNum<T>::operator/=(const BigNum<T>& other)
{
   *this = *this / other;
   return *this;
}


template <typename T>
BigNum<T> BigNum<T>::operator+(const BigNum<T>& other) const
{
    if (other.base_ != base_)
        throw std::invalid_argument("Base different");
    bool positive = true;
    if (!other.positive_ && !positive_)
        positive = false;
    else if (!positive_)
        return other + *this;
    else if (!other.positive_)
    {
        auto num = other.clone();
        num.set_positive(true);
        return *this - num;
    }
    auto num = BigNum(base_);
    num.set_positive(positive);
    size_t retenu = 0;
    size_t j = 0;
    for (; j < index_ || j < other.index_; j++)
    {
        auto tmp = retenu;
        if (j < index_)
            tmp += number_[j];
        if (j < other.index_)
            tmp += other.number_[j];
        if (tmp >= base_)
        {
            retenu = 1;
            tmp = tmp % base_;
        }
        else
            retenu = 0;
        num.set_digit(j, tmp);
    }
    if (retenu)
        num.set_digit(j, retenu);
    return num;
}

template <typename T>
BigNum<T> BigNum<T>::operator*(const BigNum<T>& other) const
{
    if (other.base_ != base_)
        throw std::invalid_argument("Base different");
    if (number_.empty() && other.number_.empty())
        return BigNum<T>(base_);
    auto result = BigNum(base_);
    auto one = BigNum(base_);
    one.set_digit(0, 1);
    BigNum<T> my(base_);
    BigNum<T> add(base_);
    if (other > *this)
    {
        my = this->clone();
        add = other.clone();
    }
    else
    {
         my = other.clone();
         add = this->clone();
    }
    my.set_positive(true);
    add.set_positive(true);
    while (my)
    {
        result = result + add;
        my = my - one;
    }
    if (!positive_ && !other.positive_)
        result.set_positive(true);
    else
        result.set_positive(positive_ && other.positive_);
    return result;
}

template <typename T>
BigNum<T> BigNum<T>::operator/(const BigNum<T>& other) const
{
    if (other.base_ != base_)
        throw std::invalid_argument("Base different");
    if (!other)
        throw std::overflow_error("Divise by 0");
    auto result = BigNum(base_);
    auto one = BigNum(base_);
    one.set_digit(0, 1);
    auto tmp = this->clone();
    while (tmp > other || tmp == other)
    {
        tmp = tmp - other;
        result = result + one;
    }
    if (!positive_ && !other.positive_)
        result.set_positive(true);
    else
        result.set_positive(positive_ && other.positive_);

    return result;
}


template <typename T>
BigNum<T> BigNum<T>::operator%(const BigNum<T>& other) const
{
    if (other.base_ != base_)
        throw std::invalid_argument("Base different");
    if (!other)
        throw std::overflow_error("Modulo 0");

    auto tmp = this->clone();
    tmp.set_positive(true);
    while (tmp > other || tmp == other)
    {
        tmp = tmp - other;
    }
    tmp.set_positive(positive_);
    return tmp;
}


}
