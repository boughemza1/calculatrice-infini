#include <iostream>
#include <fstream>
#include <sstream>

/*
 * Headers for the example
*/

#include <cstdint>

#include "base.hh"
#include "bignum.hh"
#include "AST/bin-op-node.hh"
#include "AST/un-op-node.hh"
#include "AST/number-node.hh"
#include "ast-factory.hh"



int main(int argc, char *argv[])
{

/*
    using value_t = uint32_t;
    using base_t = bistro::Base<value_t>;
    using bignum_t = bistro::BigNum<value_t>;

    auto base = base_t{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    auto b = bignum_t(10);
    b.set_digit(0, 8);
    b.set_digit(3, 8);
    b += b;
    b /= b;
    b -= b;
    b *= b;
    b %= b;
    std::cout << b.get_digit(2) << "\n"
              << b.get_digit(3) << "\n"
              << b.get_num_digits() << "\n";
    std::cout << b.get_num_digits() << "\n";
    std::cout << "b = ";
    b.print(std::cout, base);
    std::cout << std::endl;
    auto o = bignum_t(10) -  b;
    o.print(std::cout, base);
    std::cout << std::endl;
    try
    {
        b.get_digit(0);
    } catch (const std::out_of_range& e)
    {
        std::cerr << "Out of range: " << e.what() << '\n';
    }

*/
    /*auto base = base_t{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};

    std::ifstream ss("num.tes");


    auto bptr = std::make_shared<bignum_t>(ss, base);

    auto num = std::make_shared<bistro::NumberNode<bignum_t, base_t>>(bptr);
    auto minus = std::make_shared<bistro::UnOpNode<bignum_t, base_t>>(num,
        bistro::UnOpType::MINUS);
    auto plus = bistro::BinOpNode<bignum_t, base_t>(num, minus,
        bistro::BinOpType::PLUS);
    plus.print_infix(std::cout, base) << '\n';
    plus.print_pol(std::cout, base) << '\n';
    plus.eval()->print(std::cout, base) << '\n';

    std::ifstream in("test.ast");
    auto factory = bistro::ASTFactory<bignum_t, base_t>{};
    auto ast_base = factory.read(in);
    ast_base.first->print_infix(std::cout, ast_base.second) << '\n';
    ast_base.first->eval()->print(std::cout, ast_base.second) << '\n';
*/


    //AST TEST

    using value_t = int;
    using base_t = bistro::Base<value_t>;
    using bignum_t = bistro::BigNum<value_t>;

     if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " <file>\n";
        return 2;
    }

    // Create a ifstream with file "test.ast".
    std::ifstream in(argv[1]);

    if (!in.is_open())
    {
        std::cerr << "Cannot open file: " << argv[1] << '\n';
        return 3;
    }

    try
    {
        // Create the ASTFactory
        auto factory = bistro::ASTFactory<bignum_t, base_t>{};

        // Create the AST and get the root node
        auto ast_base = factory.read(in);

        // Print the expression in infix representation
        ast_base.first->print_infix(std::cout, ast_base.second) << '\n';

        // Print the evaluation of the expression
        ast_base.first->eval()->print(std::cout, ast_base.second) << '\n';
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << '\n';
        return 1;
    }

    return 0;
}
